package com.steemcraft.steemquest;

import com.steemcraft.steemquest.commands.*;
import com.steemcraft.steemquest.database.FetchData;
import com.steemcraft.steemquest.database.MySQL;
import com.steemcraft.steemquest.database.SetData;
import com.steemcraft.steemquest.listeners.*;
import com.steemcraft.steemquest.tasks.UpdateSpentBalanceTask;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import net.milkbowl.vault.economy.Economy;
//import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.ServicesManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public final class SteemQuest extends JavaPlugin {

    private static SteemQuest instance;
    public Economy economy = null;


    public HikariDataSource hikari;

    public MySQL mySQL;
    public SetData setData;
    public FetchData fetchData;

    //todo: zamiast zapisywac ilosc sbd do rozdania do pliku, mozna ja po prostu pobrac z api, odjac od tego stany kont graczy i ostatecznie tę sumkę rozdac.
    public int steemBalance = 0;
    public int spentBalance = 0;
    public int rewardsBalance = 0;

    @Override
    public void onEnable() {
        ConfigManager.registerConfig("config", "config.yml", this);
        ConfigManager.registerConfig("lootWallet", "lootWallet.yml", this);
        ConfigManager.loadAll();
        ConfigManager.saveAll();

        mySQL = new MySQL();
        setData = new SetData();
        fetchData = new FetchData();

        //mySQL.checkTable();
        //fetchData.updateInfo();
        mySQL.openConnection(); //todo: legacy.

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:mysql://" + ConfigManager.getConfig("config").getString("mysql.host") + ":" + ConfigManager.getConfig("config").getInt("mysql.port") + "/" + ConfigManager.getConfig("config").getString("mysql.database"));
        hikariConfig.setUsername(ConfigManager.getConfig("config").getString("mysql.user"));
        hikariConfig.setPassword(ConfigManager.getConfig("config").getString("mysql.password"));
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        hikari = new HikariDataSource(hikariConfig);

        Bukkit.getScheduler().runTask(this, () -> mySQL.createTable()); //run it after server fully starts

        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(new EntityListener(), this);
        pm.registerEvents(new CreatureSpawnListener(), this);
        pm.registerEvents(new PlayerDeathListener(), this);
        pm.registerEvents(new SteemQuestExperienceListener(), this);
        pm.registerEvents(new EntityDeathListener(), this);
        pm.registerEvents(new SpecialItemListener(), this);
        pm.registerEvents(new BlockBreakListener(), this);
        pm.registerEvents(new ExplosionListener(), this);

        getCommand("sethome").setExecutor(new SetHomeCommand());
        getCommand("home").setExecutor(new HomeCommand());
        getCommand("deposit").setExecutor(new DepositCommand());
        getCommand("rewards").setExecutor(new RewardsCommand());
        getCommand("withdraw").setExecutor(new WithdrawCommand());
        getCommand("balance").setExecutor(new BalanceCommand());
        getCommand("eco").setExecutor(new EcoCommand());
        getCommand("send").setExecutor(new SendCommand());
        getCommand("spawn").setExecutor(new SpawnCommand());

        registerEconomy();
        setupEconomy();

        BukkitScheduler bukkitScheduler = Bukkit.getScheduler();
        bukkitScheduler.scheduleSyncRepeatingTask(this, new UpdateSpentBalanceTask(this), 0L, 20L);
        //bukkitScheduler.scheduleAsyncRepeatingTask(this, new GetSteemBalanceTask(this), 0L, 20L);

        instance = this;
    }

    @Override
    public void onDisable() {
        //ConfigManager.save("lootWallet");
        if (hikari != null)
            hikari.close();
    }

    public static SteemQuest getPlugin() {
        return instance;
    }

    public void updateSteemBalance(){
        // curl -s --data '{"jsonrpc":"2.0", "method":"condenser_api.get_accounts", "params":[["steemcraft.com"]], "id":1}' https://api.steemit.com
        String apiUrl = ConfigManager.getConfig("config").getString("api.url");
        try{
            URL url = new URL(apiUrl + "/balance");
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            //String body = IOUtils.toString(in, encoding);

            //steemBalance = Integer.parseInt(body);
            rewardsBalance = steemBalance - spentBalance;
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }

    public int getLevel(int exp) {
        return (int) Math.floor(Math.sqrt(exp / (float) 256));
    }

    public int getExpForLevel(int level) {
        return (int) Math.pow(level, 2) * 256;
    }

    public float getExpProgress(int exp) {
        int level = getLevel(exp);
        int nextlevel = getExpForLevel(level + 1);
        int prevlevel = 0;
        if (level > 0) {
            prevlevel = getExpForLevel(level);
        }
        return ((exp - prevlevel) / (float) (nextlevel - prevlevel));
    }

    public void setTotalExperience(Player player) {
        player.setLevel(getLevel(fetchData.getXp(player.getUniqueId())));
        player.setExp(getExpProgress(fetchData.getXp(player.getUniqueId())));
        setPlayerMaxHealth(player);
    }

    public void setPlayerMaxHealth(Player player) {
        int health = 8 + (player.getLevel() / 2);
        if (health > 40) health = 40;
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
    }

    private void registerEconomy() {
        final ServicesManager sm = getServer().getServicesManager();
        sm.register(Economy.class, new SteemQuestEconomy(), this, ServicePriority.Highest);
    }


    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }

        return (economy != null);
    }

    public boolean sendToSavings(int msbd){
        String apiSecret = ConfigManager.getConfig("config").getString("api.key");
        String apiUrl = ConfigManager.getConfig("config").getString("api.url");
        try{
            URL url = new URL(apiUrl + "/toSavings/" + apiSecret + "/" + msbd);
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            //String body = IOUtils.toString(in, encoding);
            //System.out.println(body);
            //return body.equals("ok");
            return false;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

}
