package com.steemcraft.steemquest.database;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

public class SetData {

    //private MySQL mySQL = new MySQL();

    public void createPlayer(UUID uuid){
        try {
            PreparedStatement preparedStatement = SteemQuest.getPlugin().hikari.getConnection().prepareStatement("INSERT INTO players(uuid, coins, xp) VALUES(?, ?, ?)");
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setInt(2, 0);
            preparedStatement.setInt(3, 0);

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
//            try{
//                PreparedStatement sql = mySQL
//                        .getCurrentConnection()
//                        .prepareStatement("INSERT INTO `players`(`uuid`, `coins`, `xp`) VALUES(?, ?, ?)");
//                sql.setString(1, uuid.toString());
//                sql.setInt(2, 0);
//                sql.setInt(3, 0);
//                sql.execute();
//                sql.close();
//            }catch(SQLException e){
//                e.printStackTrace();
//            }
//        });
    }

    public void setCoins(UUID uuid, int coins){
        try {
            PreparedStatement preparedStatement = SteemQuest.getPlugin().hikari.getConnection().prepareStatement("UPDATE players SET coins=? WHERE uuid=?");
            preparedStatement.setInt(1, coins);
            preparedStatement.setString(2, uuid.toString());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
//           try{
//               PreparedStatement sql = mySQL
//                       .getCurrentConnection()
//                       .prepareStatement("UPDATE `players` set `coins` = ? WHERE `uuid` = ?");
//               sql.setInt(1, coins);
//               sql.setString(2, uuid.toString());
//               sql.execute();
//               sql.close();
//           }catch(SQLException e){
//               e.printStackTrace();
//           }
//        });
    }

    public void setXp(UUID uuid, int xp){
        try {
            PreparedStatement preparedStatement = SteemQuest.getPlugin().hikari.getConnection().prepareStatement("UPDATE players SET xp=? WHERE uuid=?");
            preparedStatement.setInt(1, xp);
            preparedStatement.setString(2, uuid.toString());

            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
//        Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
//            try{
//                PreparedStatement sql = mySQL
//                        .getCurrentConnection()
//                        .prepareStatement("UPDATE `players` set `xp` = ? WHERE `uuid` = ?");
//                sql.setInt(1, xp);
//                sql.setString(2, uuid.toString());
//                sql.execute();
//                sql.close();
//            }catch(SQLException e){
//                e.printStackTrace();
//            }
//        });
    }
}
