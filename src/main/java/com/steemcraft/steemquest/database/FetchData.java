package com.steemcraft.steemquest.database;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.Bukkit;

import java.sql.*;
import java.util.UUID;
import java.util.logging.Level;

public class FetchData {

    private MySQL mysql = new MySQL();

    //todo: rewrite to hikari.

    public int getCoins(UUID uuid) {
        try{
            Connection connection = SteemQuest.getPlugin().hikari.getConnection();
            Statement statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS `players`(uuid varchar(36), coins int(32), xp int(32), PRIMARY KEY (`uuid`))");
        }catch(SQLException exception){
            exception.printStackTrace();
        }

//        try {
//            ResultSet r = mysql
//                    .getCurrentConnection()
//                    .createStatement()
//                    .executeQuery("SELECT coins FROM players WHERE uuid = '" + uuid.toString() + "'");
//            if (r.next()) {
//                return r.getInt("coins");
//            }
//            r.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return -1;
    }

    public int getXp(UUID uuid) {
        try {
            ResultSet r = mysql
                    .getCurrentConnection()
                    .createStatement()
                    .executeQuery("SELECT xp FROM players WHERE uuid = '" + uuid.toString() + "'");
            if (r.next()) {
                return r.getInt("xp");
            }
            r.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public boolean isInDb(UUID uuid) {
        try {
            ResultSet r = mysql
                    .getCurrentConnection()
                    .createStatement()
                    .executeQuery("SELECT uuid FROM players WHERE uuid = '" + uuid.toString() + "'");
            boolean contains = r.next();
            r.close();
            return contains;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void updateInfo(){
        Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
            try {
                ResultSet r = mysql
                        .getCurrentConnection()
                        .createStatement()
                        .executeQuery("SELECT SUM(`coins`) AS coins FROM players");
                while(r.next()){
                    Bukkit.getLogger().log(Level.INFO, String.valueOf(r.getInt("coins")));
                    SteemQuest.getPlugin().spentBalance = r.getInt("coins");
                }
                r.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

}