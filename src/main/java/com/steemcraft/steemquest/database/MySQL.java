package com.steemcraft.steemquest.database;

import com.steemcraft.steemquest.ConfigManager;
import com.steemcraft.steemquest.SteemQuest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL {

    private Connection con;

    @Deprecated
    public synchronized void openConnection() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://"
                            + ConfigManager.getConfig("config").getString("mysql.host") + ":"
                            + ConfigManager.getConfig("config").getInt("mysql.port") + "/"
                            + ConfigManager.getConfig("config").getString("mysql.database"),
                    ConfigManager.getConfig("config").getString("mysql.user"),
                    ConfigManager.getConfig("config").getString("mysql.password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public synchronized void closeConnection() {
        try {
            if (!con.isClosed() || con != null)
                con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createTable(){
        try(Connection connection = SteemQuest.getPlugin().hikari.getConnection();
            Statement statement = connection.createStatement();){
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `players`(uuid varchar(36), coins int(32), xp int(32), PRIMARY KEY (`uuid`))");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Deprecated
    public Connection getCurrentConnection() {
        try {
            if (con == null || con.isClosed()) {
                con = DriverManager.getConnection("jdbc:mysql://"
                                + ConfigManager.getConfig("config").getString("mysql.host") + ":"
                                + ConfigManager.getConfig("config").getInt("mysql.port") + "/"
                                + ConfigManager.getConfig("config").getString("mysql.database"),
                        ConfigManager.getConfig("config").getString("mysql.user"),
                        ConfigManager.getConfig("config").getString("mysql.password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return con;
    }
}
