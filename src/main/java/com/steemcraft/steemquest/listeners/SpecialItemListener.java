package com.steemcraft.steemquest.listeners;

import org.bukkit.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SpecialItemListener implements Listener {

    @EventHandler
    void onRightClick(PlayerInteractEvent event){
        if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
            // Compass - teleports to bed (home)
            if(event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.COMPASS)){
                if(event.getPlayer().getBedSpawnLocation() != null){
                    event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount() - 1);
                    event.getPlayer().updateInventory();
                    event.getPlayer().teleport(event.getPlayer().getBedSpawnLocation());
                    event.getPlayer().playSound(event.getPlayer().getBedSpawnLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, 1, 1);
                    event.getPlayer().sendMessage(ChatColor.GRAY + "Welcome home.");
                }else{
                    event.getPlayer().sendMessage(ChatColor.GRAY + "You did not go to sleep or someone destroyed your bed.");
                }
            }
            // Watch - teleports to spawn.
            if(event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.CLOCK)){
                World spawnWorld = Bukkit.getWorld("world");
                event.getPlayer().sendMessage(ChatColor.GREEN + "Teleporting to spawn.");
                event.getPlayer().getInventory().getItemInMainHand().setAmount(event.getPlayer().getInventory().getItemInMainHand().getAmount() - 1);
                event.getPlayer().updateInventory();
                event.getPlayer().teleport(spawnWorld.getSpawnLocation());
                event.getPlayer().playSound(spawnWorld.getSpawnLocation(), Sound.ITEM_CHORUS_FRUIT_TELEPORT, 1, 1);
            }
        }
    }

}
