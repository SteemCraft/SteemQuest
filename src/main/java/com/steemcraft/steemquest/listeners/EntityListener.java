package com.steemcraft.steemquest.listeners;

import com.steemcraft.steemquest.SteemQuest;
import com.steemcraft.steemquest.database.FetchData;
import com.steemcraft.steemquest.database.SetData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class EntityListener implements Listener {

    SetData setData = new SetData();
    FetchData fetchData = new FetchData();

    @EventHandler
    void onPlayerJoin(PlayerJoinEvent event){
        if(!fetchData.isInDb(event.getPlayer().getUniqueId())){
            event.getPlayer().kickPlayer(ChatColor.YELLOW + "[SteemCraft.com]\n\n" + ChatColor.GREEN + "Welcome to SteemCraft.com\nProfile created, please join again.");
            setData.createPlayer(event.getPlayer().getUniqueId());
            return;
        }

        Player player = event.getPlayer();

        player.sendMessage(ChatColor.GRAY + "Welcome to SteemCraft!");
        player.sendMessage(ChatColor.GRAY + "Check our wiki to learn more about the server: " + ChatColor.YELLOW + "steemcraft.com");
        player.sendMessage("");

        SteemQuest.getPlugin().setTotalExperience(player);
        SteemQuest.getPlugin().setPlayerMaxHealth(player);
    }
}
