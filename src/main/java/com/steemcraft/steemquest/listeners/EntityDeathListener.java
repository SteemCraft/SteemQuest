package com.steemcraft.steemquest.listeners;

import com.steemcraft.steemquest.LootWallet;
import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.metadata.MetadataValue;

import java.util.List;

public class EntityDeathListener implements Listener {

    @EventHandler
    void onEntityDeath(EntityDeathEvent event) {
        LivingEntity livingEntity = event.getEntity();
        event.setDroppedExp(0);
        //we can use entity metadata "steemlevel" - CreatureSpawnListener line 54
        if (livingEntity.hasMetadata("steemlevel")) {
            List<MetadataValue> entityLevelMetadata = livingEntity.getMetadata("steemlevel");
            int entityLevel = 1;
            if (!entityLevelMetadata.isEmpty()) {
                entityLevel = entityLevelMetadata.get(0).asInt();
            }

            if (livingEntity instanceof Monster || livingEntity instanceof Ghast) {
                if (livingEntity.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
                    EntityDamageByEntityEvent damage = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();
                    Entity damager = damage.getDamager();

                    if (damager instanceof Arrow) {
                        Arrow arrow = (Arrow) damager;
                        if(arrow.getShooter() instanceof LivingEntity){
                            damager = (LivingEntity) arrow.getShooter();
                        }
                    }
                    if (damager instanceof Player) {
                        Player player = (Player) damager;

                        if (entityLevel >= 1) {
                            //Experience!
                            SteemQuest.getPlugin().setData.setXp(player.getUniqueId(), SteemQuest.getPlugin().fetchData.getXp(player.getUniqueId()) + (entityLevel * 4));
                            SteemQuest.getPlugin().setTotalExperience(player);

                            int money = Math.min(SteemQuest.random(1, entityLevel), SteemQuest.random(1, entityLevel));
                            if (livingEntity instanceof Ghast) {
                                money = money * 2;
                            }

                            int dice = SteemQuest.random(1, 100);
                            if (LootWallet.getBalance() > 250) {
                                dice = SteemQuest.random(1, 25);
                            }
                            if (LootWallet.getBalance() > 1000) {
                                dice = SteemQuest.random(1, 15);
                            }
                            if (LootWallet.getBalance() > 2000) {
                                dice = 7;
                            }

                            if (dice == 7) {
                                if (LootWallet.getBalance() >= money) {
                                    player.sendMessage(ChatColor.GREEN + "You received " + money + " mSBD.");
                                    player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 20, 1);
                                    LootWallet.sendToPlayer(player, money);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}
