package com.steemcraft.steemquest.listeners;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class ExplosionListener implements Listener {

    @EventHandler
    void onExplosion(EntityExplodeEvent event) {
        if (event.getEntityType().equals(EntityType.MINECART_TNT) ||
                event.getEntityType().equals(EntityType.PRIMED_TNT) ||
                event.getEntityType().equals(EntityType.CREEPER)) {
            event.setYield(0);
            event.setCancelled(true);
        }
    }
}
