package com.steemcraft.steemquest.listeners;

import com.steemcraft.steemquest.SteemQuest;
import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CreatureSpawnListener implements Listener {

    @EventHandler
    void onCreatureSpawn(CreatureSpawnEvent event) {
        LivingEntity livingEntity = event.getEntity();
        int minLevel = 1;
        int maxLevel = 20;
        if (livingEntity.getWorld().getName().endsWith("_nether")) {
            minLevel = 20;
            maxLevel = 40;
        }else if (livingEntity.getWorld().getName().endsWith("_the_end")) {
            minLevel = 40;
            maxLevel = 60;
        }

        // more distance from spawn = higher level.
        int distanceToSpawn = (int) event.getLocation().distance(event.getLocation().getWorld().getSpawnLocation());

        int creatureLevel = SteemQuest.random(minLevel, Math.max(minLevel, (Math.min(maxLevel, (distanceToSpawn / 96)))));

        if (livingEntity instanceof Monster || livingEntity instanceof Ghast) {
            if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.SPAWNER)) {
                event.setCancelled(true);
            } else if (event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.SPAWNER_EGG)) {
                event.setCancelled(true);
            } else {
                if (creatureLevel < 1) {
                    creatureLevel = 1;
                }

                livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(1 + creatureLevel);
                livingEntity.setHealth(1 + creatureLevel); //heal creature!
                livingEntity.setMetadata("steemlevel", new FixedMetadataValue(SteemQuest.getPlugin(), creatureLevel));
                livingEntity.setCustomName(WordUtils.capitalizeFully(livingEntity.getType().name().replace("_", " ")) + ChatColor.YELLOW + " (Level " + creatureLevel + ")" + ChatColor.RESET);

                //Some potions!
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, Integer.MAX_VALUE, 2), true);
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, Integer.MAX_VALUE, 2), true);
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, Integer.MAX_VALUE, 2), true);
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, Integer.MAX_VALUE, 2), true);
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 2), true);
                if (SteemQuest.random(1, 128) < creatureLevel)
                    livingEntity.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 2), true);

                if (livingEntity instanceof Zombie || livingEntity instanceof Skeleton) {
                    useRandomEquipment(livingEntity, creatureLevel);
                }

                if (livingEntity instanceof Zombie || livingEntity instanceof WitherSkeleton) {
                    useRandomSword(livingEntity, creatureLevel);
                }

                if (livingEntity instanceof Creeper && SteemQuest.random(0, 100) < creatureLevel) {
                    ((Creeper) livingEntity).setPowered(true);
                }

                if (livingEntity instanceof PigZombie) {
                    PigZombie pigZombie = (PigZombie) livingEntity;
                    pigZombie.setAngry(true);
                }

                if (livingEntity instanceof Skeleton && !(livingEntity instanceof WitherSkeleton)) {
                    ItemStack bow = new ItemStack(Material.BOW);
                    randomEnchantItem(bow, creatureLevel);
                }

                if (livingEntity instanceof Ghast) {
                    livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(creatureLevel * 3);
                    livingEntity.setHealth(creatureLevel * 3); //heal creature!
                }

                if (livingEntity instanceof Enderman) {
                    //TODO: i should make endermans harder to kill :)
                    livingEntity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(creatureLevel * 4);
                    livingEntity.setHealth(creatureLevel * 4); //heal creature!
                }
            }
        }

    }

    private void useRandomSword(LivingEntity entity, int level) {
        ItemStack sword = null;
        if (SteemQuest.random(0, 16) < level) sword = new ItemStack(Material.IRON_AXE);
        if (SteemQuest.random(0, 32) < level) sword = new ItemStack(Material.WOODEN_SWORD);
        if (SteemQuest.random(0, 64) < level) sword = new ItemStack(Material.IRON_SWORD);
        if (SteemQuest.random(0, 96) < level) sword = new ItemStack(Material.GOLDEN_SWORD);
        if (SteemQuest.random(0, 128) < level) sword = new ItemStack(Material.DIAMOND_SWORD);
        if (sword != null) {
            randomEnchantItem(sword, level);
            entity.getEquipment().setItemInMainHand(sword);
        }
    }

    private void useRandomEquipment(LivingEntity entity, int level) {
        ItemStack helmet = null;
        if (SteemQuest.random(0, 16) < level) helmet = new ItemStack(Material.LEATHER_HELMET);
        if (SteemQuest.random(0, 32) < level) helmet = new ItemStack(Material.CHAINMAIL_HELMET);
        if (SteemQuest.random(0, 64) < level) helmet = new ItemStack(Material.IRON_HELMET);
        if (SteemQuest.random(0, 128) < level) helmet = new ItemStack(Material.DIAMOND_HELMET);
        if (helmet != null) {
            randomEnchantItem(helmet, level);
            entity.getEquipment().setHelmet(helmet);
        }

        ItemStack chestplate = null;
        if (SteemQuest.random(0, 16) < level) chestplate = new ItemStack(Material.LEATHER_CHESTPLATE);
        if (SteemQuest.random(0, 32) < level) chestplate = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
        if (SteemQuest.random(0, 64) < level) chestplate = new ItemStack(Material.IRON_CHESTPLATE);
        if (SteemQuest.random(0, 128) < level) chestplate = new ItemStack(Material.DIAMOND_CHESTPLATE);
        if (chestplate != null) {
            randomEnchantItem(chestplate, level);
            entity.getEquipment().setChestplate(chestplate);
        }

        ItemStack leggings = null;
        if (SteemQuest.random(0, 16) < level) leggings = new ItemStack(Material.LEATHER_LEGGINGS);
        if (SteemQuest.random(0, 32) < level) leggings = new ItemStack(Material.CHAINMAIL_LEGGINGS);
        if (SteemQuest.random(0, 64) < level) leggings = new ItemStack(Material.IRON_LEGGINGS);
        if (SteemQuest.random(0, 128) < level) leggings = new ItemStack(Material.DIAMOND_LEGGINGS);
        if (leggings != null) {
            randomEnchantItem(leggings, level);
            entity.getEquipment().setLeggings(leggings);
        }

        ItemStack boots = null;
        if (SteemQuest.random(0, 16) < level) boots = new ItemStack(Material.LEATHER_BOOTS);
        if (SteemQuest.random(0, 32) < level) boots = new ItemStack(Material.CHAINMAIL_BOOTS);
        if (SteemQuest.random(0, 64) < level) boots = new ItemStack(Material.IRON_BOOTS);
        if (SteemQuest.random(0, 128) < level) boots = new ItemStack(Material.DIAMOND_BOOTS);
        if (boots != null) {
            randomEnchantItem(boots, level);
            entity.getEquipment().setBoots(boots);
        }
    }

    // Random Enchantment
    private void randomEnchantItem(ItemStack item, int level) {
        ItemMeta meta = item.getItemMeta();
        Enchantment enchantment = null;
        //todo: paper only .-.
        //String itemName = item.getI18NDisplayName();
        //if (itemName.contains("Bow")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.ARROW_DAMAGE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.ARROW_INFINITE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.ARROW_KNOCKBACK;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.ARROW_FIRE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
        //}
        //if (itemName.contains("Sword")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DAMAGE_ALL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DAMAGE_ARTHROPODS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DAMAGE_UNDEAD;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.FIRE_ASPECT;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.SWEEPING_EDGE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.KNOCKBACK;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.LOOT_BONUS_MOBS;
        //}
        //if (itemName.contains("Shovel") || itemName.contains("Axe") || itemName.contains("Pickaxe")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.LOOT_BONUS_BLOCKS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DIG_SPEED;
        //}
        //if (itemName.contains("Cap") || itemName.contains("Helmet")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.THORNS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.WATER_WORKER;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_EXPLOSIONS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FALL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_PROJECTILE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FIRE;
        //}
        //if (itemName.contains("Tunic") || itemName.contains("Chestplate")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.THORNS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_EXPLOSIONS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FALL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_PROJECTILE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FIRE;
        //}
        //if (itemName.contains("Pants") || itemName.contains("Leggings")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.THORNS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_EXPLOSIONS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FALL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_PROJECTILE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FIRE;
        //}
        //if (itemName.contains("Boots")) {
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.DURABILITY;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.THORNS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_ENVIRONMENTAL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_EXPLOSIONS;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FALL;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_PROJECTILE;
            if (SteemQuest.random(0, 128) < level) enchantment = Enchantment.PROTECTION_FIRE;
        //}

        if (enchantment != null) {
            meta.addEnchant(enchantment, SteemQuest.random(enchantment.getStartLevel(), enchantment.getMaxLevel()), true);
            item.setItemMeta(meta);

        }
    }

}
