package com.steemcraft.steemquest.listeners;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class SteemQuestExperienceListener implements Listener{

    @EventHandler
    void onExperienceChange(PlayerExpChangeEvent event) {
        event.setAmount(0);
    }

    // https://github.com/bitquest/bitquest/blob/master/src/main/java/com/bitquest/bitquest/EntityEvents.java#L225-L234
    @EventHandler
    void onEnchantItemEvent(EnchantItemEvent event){
        event.getEnchanter().setLevel(event.getEnchanter().getLevel() + event.whichButton() + 1);

    }

    @EventHandler
    void onAnvil(InventoryClickEvent event){
        if(event.getClickedInventory() != null){ //null pointers when clicking outside window :/
            if(event.getClickedInventory().getType().equals(InventoryType.ANVIL)){
                Bukkit.getScheduler().scheduleSyncDelayedTask(SteemQuest.getPlugin(), () -> { //run it after tick (~0.05s)
                    if(event.getWhoClicked() instanceof Player){
                        SteemQuest.getPlugin().setTotalExperience((Player) event.getWhoClicked());
                    }
                }, 1L);
            }
        }
    }

}
