package com.steemcraft.steemquest.listeners;

import com.steemcraft.steemquest.LootWallet;
import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreakListener implements Listener {

    // Happy numbers are great
    // https://en.wikipedia.org/wiki/Happy_number

    @EventHandler
    void onBlockBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        if(player.getGameMode() != GameMode.CREATIVE){
            Material material = event.getBlock().getType();
            // MINING
            if(player.getInventory().getItemInMainHand() != null){
                if(!player.getInventory().getItemInMainHand().getEnchantments().containsKey(Enchantment.SILK_TOUCH)){
                    if(material.equals(Material.DIAMOND_ORE) ||
                            material.equals(Material.COAL_ORE) ||
                            material.equals(Material.LAPIS_ORE) ||
                            material.equals(Material.REDSTONE_ORE) ||
                            material.equals(Material.EMERALD_ORE)){
                        int twoHundredSidedGoldenDice = SteemQuest.random(1, 200);
                        if(twoHundredSidedGoldenDice == 7){ //seven is a happy number
                            int totallyRandomNumber = SteemQuest.random(1, 4);
                            if(LootWallet.getBalance() < totallyRandomNumber){
                                totallyRandomNumber = LootWallet.getBalance();
                            }
                            if(totallyRandomNumber > 0){
                                LootWallet.sendToPlayer(player, totallyRandomNumber);
                                player.sendMessage(ChatColor.GREEN + "Received " + totallyRandomNumber + "mSBD from mining!");
                            }
                        }
                    }
                }
            }

            // FARMING
            //wheat, carrot, potato, beetroot - https://minecraft.gamepedia.com/Java_Edition_data_values#Crops
            Block block = event.getBlock();
            if((material.equals(Material.LEGACY_CROPS) && block.getData() == 7) || //todo: 1.13
                    (material.equals(Material.CARROT) && block.getData() == 7) ||
                    (material.equals(Material.POTATO) && block.getData() == 7) ||
                    (material.equals(Material.BEETROOT) && block.getData() == 3)){
                int fiveHundredSidedFarmDice = SteemQuest.random(1, 500);
                if(fiveHundredSidedFarmDice == 392){ //another happy number!
                    int totallyRandomNumber = SteemQuest.random(1, 4);
                    if(LootWallet.getBalance() < totallyRandomNumber){
                        totallyRandomNumber = LootWallet.getBalance();
                    }
                    if(totallyRandomNumber > 0){
                        LootWallet.sendToPlayer(player, totallyRandomNumber);
                        player.sendMessage(ChatColor.GREEN + "Received " + totallyRandomNumber + "mSBD from farming!");
                    }
                }
            }
        }
    }
}
