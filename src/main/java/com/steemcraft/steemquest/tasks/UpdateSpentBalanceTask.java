package com.steemcraft.steemquest.tasks;

import com.steemcraft.steemquest.SteemQuest;

public class UpdateSpentBalanceTask implements Runnable {

    private final SteemQuest plugin;

    public UpdateSpentBalanceTask(SteemQuest plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        plugin.fetchData.updateInfo();
    }

}
