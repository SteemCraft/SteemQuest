package com.steemcraft.steemquest.tasks;

import com.steemcraft.steemquest.SteemQuest;

public class GetSteemBalanceTask implements Runnable {

    private final SteemQuest plugin;

    public GetSteemBalanceTask(SteemQuest plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        plugin.updateSteemBalance();
    }

}
