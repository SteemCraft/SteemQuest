package com.steemcraft.steemquest;

import org.bukkit.entity.Player;

@Deprecated
public class LootWallet {

    public static int getBalance(){
        return ConfigManager.getConfig("lootWallet").getInt("balance");
    }

    public static void set(int newBalance){
        ConfigManager.getConfig("lootWallet").set("balance", newBalance);
        if(getBalance() < 0){
            set(0);
        }
        ConfigManager.save("lootWallet");
    }

    public static void add(int howMuch){
        set(getBalance() + howMuch);
    }

    public static void remove(int howMuch){
        set(getBalance() - howMuch);
    }

    public static void sendToPlayer(Player player, int howMuch){
        SteemQuest.getPlugin().economy.depositPlayer(player, howMuch);
        remove(howMuch);
    }

    public static void sendFromPlayer(Player player, int howMuch){
        SteemQuest.getPlugin().economy.withdrawPlayer(player, howMuch);
        add(howMuch);
    }
}
