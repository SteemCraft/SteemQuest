package com.steemcraft.steemquest.commands;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*
TODO przelewa dana ilosc mSBD do innego gracza, sprawdzac czy taki gracz juz istnieje!
 */
public class SendCommand implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(args.length == 2){
                String to = args[0];
                String amount = args[1];
                if(Bukkit.getPlayer(to).isOnline()){
                    if(Double.parseDouble(amount) != Double.NaN){
                        Player another = Bukkit.getPlayer(to);
                        Double send = Double.parseDouble(amount);
                        if(SteemQuest.getPlugin().economy.getBalance(player) > send){
                            SteemQuest.getPlugin().economy.withdrawPlayer(player, send);
                            SteemQuest.getPlugin().economy.depositPlayer(another, send);
                            player.sendMessage(ChatColor.GREEN + "Sent " + send + "mSBD to " + another.getName());
                            another.sendMessage(ChatColor.GREEN + "Received " + send + "mSBD from " + player.getName());
                        }else{
                            player.sendMessage(ChatColor.RED + "Not enogh money.");
                        }
                    }else{
                        player.sendMessage(ChatColor.RED + "Type correct number.");
                    }
                }else{
                    player.sendMessage(ChatColor.RED + "Player is offline.");
                }
            }else{
                player.sendMessage(ChatColor.RED + "Correct usage: /send <player> <amount>");
            }
        }
        return true;
    }
}
