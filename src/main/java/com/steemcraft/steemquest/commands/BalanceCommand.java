package com.steemcraft.steemquest.commands;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BalanceCommand implements CommandExecutor{

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        //TODO: see other player balance
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            player.sendMessage(ChatColor.GRAY + "Balance: " + ChatColor.GOLD + SteemQuest.getPlugin().economy.getBalance(player) + "mSBD.");
        }
        return true;
    }

}
