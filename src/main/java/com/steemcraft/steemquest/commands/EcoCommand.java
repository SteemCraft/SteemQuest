package com.steemcraft.steemquest.commands;

import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.*;

public class EcoCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(commandSender instanceof ConsoleCommandSender || commandSender instanceof RemoteConsoleCommandSender){
            if(args.length != 0){
                if(args[0].equals("add")){
                    if(args.length == 3){
                        OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
                        SteemQuest.getPlugin().economy.depositPlayer(player, Double.parseDouble(args[2]));
                        commandSender.sendMessage("added");
                    }else{
                        commandSender.sendMessage("add <nickname> <money>");
                    }
                }
                if(args[0].equals("take")){
                    if(args.length == 3){
                        OfflinePlayer player = Bukkit.getOfflinePlayer(args[1]);
                        SteemQuest.getPlugin().economy.withdrawPlayer(player, Double.parseDouble(args[2]));
                        commandSender.sendMessage("taken");
                    }else{
                        commandSender.sendMessage("take <nickname> <money>");
                    }
                }
            }else{
                commandSender.sendMessage("add, take");
            }
        }
        return true;
    }

}
