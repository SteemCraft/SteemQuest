package com.steemcraft.steemquest.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpawnCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            player.sendMessage(ChatColor.GRAY + "If you want to teleport to server spawn, right click with watch.");
            player.sendMessage(ChatColor.GRAY + "Spawn location (x, y, z): " + ChatColor.YELLOW + Bukkit.getWorld("world").getSpawnLocation().getX() + ", " + Bukkit.getWorld("world").getSpawnLocation().getY() + ", " + Bukkit.getWorld("world").getSpawnLocation().getZ());
        }
        return true;
    }

}
