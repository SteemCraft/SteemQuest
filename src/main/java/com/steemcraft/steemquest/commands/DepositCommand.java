package com.steemcraft.steemquest.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class DepositCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            if(args.length == 1){
                if(StringUtils.isNumeric(args[0])){
                    String to = "steemcraft.com";
                    double sbd = (Double.parseDouble(args[0]))/1000;
                    String uuid = player.getUniqueId().toString();
                    commandSender.sendMessage(ChatColor.GRAY + "You can deposit Steem Dollars here:");
                    commandSender.sendMessage(ChatColor.YELLOW + "https://steemconnect.com/sign/transfer?to=" + to + "&amount=" + sbd + "%20SBD&memo=" + uuid);
                }else{
                    commandSender.sendMessage(ChatColor.RED + "Argument must be numeric.");
                }
            }else{
                commandSender.sendMessage(ChatColor.RED + "Correct usage: /deposit <amount in mSBD>");
            }
        }
        return true;
    }

}
