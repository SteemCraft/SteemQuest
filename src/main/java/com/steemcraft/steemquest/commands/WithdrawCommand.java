package com.steemcraft.steemquest.commands;

import com.steemcraft.steemquest.ConfigManager;
import com.steemcraft.steemquest.SteemQuest;
//import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class WithdrawCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(args.length == 0){
            sendHelp(commandSender);
            return true;
        }
        if(args.length == 1){
            sendHelp(commandSender);
            return true;
        }
        if(args.length == 2){
            if(StringUtils.isNumeric(args[0])){
                int toWithdraw = Integer.parseInt(args[0]);
                if(toWithdraw <= 5){
                    commandSender.sendMessage(ChatColor.RED + "Amount must higher than 5mSBD");
                    return true;
                }else{
                    if(commandSender instanceof Player){
                        Player player = (Player) commandSender;
                        if (SteemQuest.getPlugin().economy.has(player, toWithdraw)) {
                            commandSender.sendMessage(ChatColor.GRAY + "Connecting to payout server, wait a second :)");
                            Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
                                if(withdraw(player, toWithdraw, args[1])){
                                    commandSender.sendMessage(ChatColor.GREEN + "The payment has been made.");
                                    Bukkit.getScheduler().runTaskAsynchronously(SteemQuest.getPlugin(), () -> {
                                        SteemQuest.getPlugin().sendToSavings(3); //send fee to savings :p
                                    });
                                }else{
                                    commandSender.sendMessage(ChatColor.RED + "An error occurred while connecting to the payout server. Funds will return to your account in 5 seconds.");
                                }
                            });

                        }else{
                            commandSender.sendMessage(ChatColor.RED + "Not enough funds.");
                            return true;
                        }
                    }else{
                        commandSender.sendMessage(ChatColor.RED + "Must be player.");
                        return true;
                    }
                }
            }else{
                commandSender.sendMessage(ChatColor.RED + "Amount must be numeric.");
                return true;
            }
        }
        return true;
    }

    private void sendHelp(CommandSender commandSender){
        commandSender.sendMessage(ChatColor.GRAY + "Minimal withdraw amount: 5 mSBD (0.005 SBD / 0.005$)");
        commandSender.sendMessage(ChatColor.GRAY + "/withdraw <amount in mSBD> <steem account>");
        commandSender.sendMessage(ChatColor.GRAY + "Example: /withdraw 320 artur9010");
        commandSender.sendMessage(ChatColor.RED + "If you type incorrect steem username, YOUR FUNDS WILL BE LOST.");
        commandSender.sendMessage(ChatColor.RED + "Also there is a 3mSBD FEE TO AVOID US RUNNIG OUT A BANDWIDTH (SPAMMING).");
    }

    private boolean withdraw(Player player, final int msbd, String steemUser){
        String apiSecret = ConfigManager.getConfig("config").getString("api.key");
        String apiUrl = ConfigManager.getConfig("config").getString("api.url");
        steemUser = steemUser.toLowerCase();
        SteemQuest.getPlugin().economy.withdrawPlayer(player, msbd);
        try{
            int msbd2 = msbd - 3;
            URL url = new URL(apiUrl + "/withdraw/" + apiSecret + "/" + steemUser + "/" + msbd2);
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            String encoding = con.getContentEncoding();
            encoding = encoding == null ? "UTF-8" : encoding;
            //String body = IOUtils.toString(in, encoding);
            //System.out.println(body);
            //player.sendMessage(body);
            //return body.equals("ok");
            return false;
        }catch(Exception ex){
            Bukkit.getScheduler().runTaskLater(SteemQuest.getPlugin(), () -> SteemQuest.getPlugin().economy.depositPlayer(player, msbd), 100L);
            ex.printStackTrace();
            return false;
        }
    }
}
