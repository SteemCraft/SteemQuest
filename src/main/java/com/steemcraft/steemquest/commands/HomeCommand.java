package com.steemcraft.steemquest.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HomeCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            if(player.getBedSpawnLocation() != null){
                player.sendMessage(ChatColor.GRAY + "If you want to teleport to your home (bed), right click with compass.");
                player.sendMessage(ChatColor.GRAY + "Bed location (x, y, z): " + ChatColor.YELLOW + player.getBedSpawnLocation().getX() + ", " + player.getBedSpawnLocation().getY() + ", " + player.getBedSpawnLocation().getZ());
            }else{
                player.sendMessage(ChatColor.RED + "You are homeless. Go sleep!");
            }
        }
        return true;
    }

}
