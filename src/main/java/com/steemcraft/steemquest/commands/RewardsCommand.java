package com.steemcraft.steemquest.commands;

import com.steemcraft.steemquest.LootWallet;
import com.steemcraft.steemquest.SteemQuest;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RewardsCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        commandSender.sendMessage(ChatColor.GRAY + "Current rewards pool: " + ChatColor.GOLD + SteemQuest.getPlugin().rewardsBalance + " mSBD");
        return true;
    }
}
