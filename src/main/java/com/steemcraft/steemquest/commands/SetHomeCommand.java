package com.steemcraft.steemquest.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class SetHomeCommand implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args){
        commandSender.sendMessage(ChatColor.GRAY + "There is no /sethome! Use beds :)");
        commandSender.sendMessage(ChatColor.GRAY + "You can teleport to your bed with compass.");
        return true;
    }

}
