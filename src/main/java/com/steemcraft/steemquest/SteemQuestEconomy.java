package com.steemcraft.steemquest;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.List;

public class SteemQuestEconomy implements Economy {
    @Override
    public EconomyResponse bankBalance(String arg0) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse bankDeposit(String arg0, double arg1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse bankHas(String arg0, double arg1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse bankWithdraw(String player, double amount) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse createBank(String arg0, String arg1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public boolean createPlayerAccount(String arg0) {
        return false;
    }

    @Override
    public String currencyNamePlural() {
        return "mSBD";
    }

    @Override
    public String currencyNameSingular() {
        return "mSBD";
    }

    @Override
    public EconomyResponse deleteBank(String arg0) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse depositPlayer(String player, double amount) {
//        SteemQuestPlayer steemQuestPlayer = new SteemQuestPlayer(Bukkit.getOfflinePlayer(player).getUniqueId());
//        steemQuestPlayer.setBalance(steemQuestPlayer.getBalance() + amount);
        SteemQuest.getPlugin().setData.setCoins(Bukkit.getOfflinePlayer(player).getUniqueId(), (int) (SteemQuest.getPlugin().fetchData.getCoins(Bukkit.getOfflinePlayer(player).getUniqueId()) + amount));
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "");
    }

    @Override
    public String format(double amount) {
        return amount + "mSBD";
    }

    @Override
    public int fractionalDigits() {
        return 0;
    }

    @Override
    public double getBalance(String player) {
        try {
            return SteemQuest.getPlugin().fetchData.getCoins(Bukkit.getOfflinePlayer(player).getUniqueId());
            //return new SteemQuestPlayer(Bukkit.getOfflinePlayer(player).getUniqueId()).getBalance();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public List<String> getBanks() {
        return null;
    }

    @Override
    public String getName() {
        return "SteemCraftEconomy";
    }

    @Override
    public boolean has(String player, double amount) {
        double pAmount = 0;
        try {
            //pAmount = new SteemQuestPlayer(Bukkit.getOfflinePlayer(player).getUniqueId()).getBalance();
            pAmount = SteemQuest.getPlugin().fetchData.getCoins(Bukkit.getOfflinePlayer(player).getUniqueId());
        } catch (Exception ignored) {
        }
        return pAmount >= amount;
    }

    @Override
    public boolean hasAccount(String player) {
        try {
            //return new SteemQuestPlayer(Bukkit.getOfflinePlayer(player).getUniqueId()).getUserFile() != null;
            return SteemQuest.getPlugin().fetchData.isInDb(Bukkit.getOfflinePlayer(player).getUniqueId());
        } catch (Exception ignored) {
        }

        return false;
    }

    @Override
    public boolean hasBankSupport() {
        return false;
    }

    @Override
    public EconomyResponse isBankMember(String arg0, String arg1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public EconomyResponse isBankOwner(String arg0, String arg1) {
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.NOT_IMPLEMENTED, "Not implemented yet.");
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public EconomyResponse withdrawPlayer(String player, double amount) {
        try {
            SteemQuest.getPlugin().setData.setCoins(Bukkit.getOfflinePlayer(player).getUniqueId(), (int) (SteemQuest.getPlugin().fetchData.getCoins(Bukkit.getOfflinePlayer(player).getUniqueId()) - amount));
            //SteemQuestPlayer steemQuestPlayer = new SteemQuestPlayer(Bukkit.getOfflinePlayer(player).getUniqueId());
            //steemQuestPlayer.setBalance(steemQuestPlayer.getBalance() - amount);
            return new EconomyResponse(0, 0, EconomyResponse.ResponseType.SUCCESS, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new EconomyResponse(0, 0, EconomyResponse.ResponseType.FAILURE, "");
    }

    @Override
    public boolean createPlayerAccount(String player, String world) {
        return createPlayerAccount(player);
    }

    @Override
    public EconomyResponse depositPlayer(String player, String world, double amount) {
        return depositPlayer(player, amount);
    }

    @Override
    public double getBalance(String player, String world) {
        return getBalance(player);
    }

    @Override
    public boolean has(String player, String world, double amount) {
        return has(player, amount);
    }

    @Override
    public boolean hasAccount(String player, String world) {
        return hasAccount(player);
    }

    @Override
    public EconomyResponse withdrawPlayer(String player, String world, double amount) {
        return withdrawPlayer(player, amount);
    }

    @Override
    public EconomyResponse createBank(String bank, OfflinePlayer player) {
        return createBank(bank, player.getName());
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player) {
        return createPlayerAccount(player.getName());
    }

    @Override
    public boolean createPlayerAccount(OfflinePlayer player, String world) {
        return createPlayerAccount(player.getName(), world);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
        return depositPlayer(player.getName(), amount);
    }

    @Override
    public EconomyResponse depositPlayer(OfflinePlayer player, String world, double amount) {
        return depositPlayer(player.getName(), world, amount);
    }

    @Override
    public double getBalance(OfflinePlayer player) {
        return getBalance(player.getName());
    }

    @Override
    public double getBalance(OfflinePlayer player, String world) {
        return getBalance(player.getName(), world);
    }

    @Override
    public boolean has(OfflinePlayer player, double amount) {
        return has(player.getName(), amount);
    }

    @Override
    public boolean has(OfflinePlayer player, String world, double amount) {
        return has(player.getName(), world, amount);
    }

    @Override
    public boolean hasAccount(OfflinePlayer player) {
        return hasAccount(player.getName());
    }

    @Override
    public boolean hasAccount(OfflinePlayer player, String world) {
        return hasAccount(player.getName(), world);
    }

    @Override
    public EconomyResponse isBankMember(String bank, OfflinePlayer player) {
        return isBankMember(bank, player.getName());
    }

    @Override
    public EconomyResponse isBankOwner(String bank, OfflinePlayer player) {
        return isBankOwner(bank, player.getName());
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
        return withdrawPlayer(player.getName(), amount);
    }

    @Override
    public EconomyResponse withdrawPlayer(OfflinePlayer player, String world, double amount) {
        return withdrawPlayer(player.getName(), world, amount);
    }

}