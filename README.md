# SteemQuest - official SteemCraft Minecraft Plugin
```
PLAY NOW:
Minecraft: Java Edition 1.12.2 - play.steemcraft.com
```

## Building
Make sure you have Java 8 JDK and Maven installed in your system.

Clone repository:
```
git clone https://gitlab.com/SteemCraft/SteemQuest
```

Go to the directory:
```
cd SteemQuest
```

And build it using maven:
```
mvn
```

## Optional requirements
* [payments](https://gitlab.com/SteemCraft/steemcraft_api) - allows players to deposit and withdraw Steem Dollars

## Special thanks to
* JetBrains for creating great development software <3